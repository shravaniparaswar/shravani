function add() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    let ans = document.getElementById("ans");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c + d;
    ans.innerHTML = "The addition is : </br>" + res.toString();
}
function sub() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    let ans = document.getElementById("ans");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c - d;
    ans.innerHTML = "The Subtraction is : </br>" + res.toString();
}
function multi() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    let ans = document.getElementById("ans");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c * d;
    ans.innerHTML = "The Multiplication is : </br>" + res.toString();
}
function div() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    let ans = document.getElementById("ans");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c / d;
    ans.innerHTML = "The Division is : </br>" + res.toString();
}
function sine() {
    let t3 = document.getElementById("t3");
    let ans = document.getElementById("ans");
    var c = parseFloat(t3.value);
    var res = Math.sin(c);
    ans.innerHTML = "The sine is : </br>" + res.toString();
}
function cosine() {
    let t3 = document.getElementById("t3");
    let ans = document.getElementById("ans");
    var c = parseFloat(t3.value);
    var res = Math.cos(c);
    ans.innerHTML = "The cosine is : </br>" + res.toString();
}
function tangent() {
    let t3 = document.getElementById("t3");
    let ans = document.getElementById("ans");
    var c = parseFloat(t3.value);
    var res = Math.tan(c);
    ans.innerHTML = "The tangent is : </br>" + res.toString();
}
function Sqrt() {
    let t3 = document.getElementById("t3");
    let ans = document.getElementById("ans");
    var c = parseFloat(t3.value);
    var res = Math.sqrt(c);
    ans.innerHTML = "The square root is : </br>" + res.toString();
}
function Pow() {
    let t1 = document.getElementById("t3");
    let t2 = document.getElementById("t3");
    let ans = document.getElementById("ans");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = Math.pow(c, d);
    ans.innerHTML = "The power is : </br>" + res.toString();
}
//# sourceMappingURL=func.js.map