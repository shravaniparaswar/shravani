function add()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c+d;
    ans.innerHTML="The addition is : </br>"+res.toString();
}

function sub()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c-d;
    ans.innerHTML="The Subtraction is : </br>"+res.toString();
}

function multi()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c*d;
    ans.innerHTML="The Multiplication is : </br>"+res.toString();
}

function div()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c/d;
    ans.innerHTML="The Division is : </br>"+res.toString();
}

function sine()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t3.value);
    var res:number=Math.sin(c);
    ans.innerHTML="The sine is : </br>"+res.toString();
}

function cosine()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t3.value);
    var res:number=Math.cos(c);
    ans.innerHTML="The cosine is : </br>"+res.toString();
}

function tangent()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t3.value);
    var res:number=Math.tan(c);
    ans.innerHTML="The tangent is : </br>"+res.toString();
}

function Sqrt()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t3.value);
    var res:number=Math.sqrt(c);
    ans.innerHTML="The square root is : </br>"+res.toString();
}

function Pow()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=Math.pow(c,d);
    ans.innerHTML="The power is : </br>"+res.toString();
}






