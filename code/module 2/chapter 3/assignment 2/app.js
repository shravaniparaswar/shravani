function tri() {
    var a1 = document.getElementById("t1");
    var b1 = document.getElementById("t2");
    var c1 = document.getElementById("t3");
    var a = parseFloat(a1.value);
    var b = parseFloat(b1.value);
    var c = parseFloat(c1.value);
    if ((a == b) && (b == c)) {
        document.getElementById("t4").innerHTML = "It is an equivalent triangle.";
    }
    else if ((a != b || b != c) && (a == b || b == c || c == a)) {
        if (((a * a) == (b * b) + (c * c)) || ((b * b) == (c * c) + (a * a)) || ((c * c) == (a * a) + (b * b))) {
            document.getElementById("t5").innerHTML = "It is a right angled triangle.";
        }
        document.getElementById("t4").innerHTML = "It is an isosceles triangle.";
    }
    else if ((a != b) && (b != c)) {
        if (((a * a) == (b * b) + (c * c)) || ((b * b) == (c * c) + (a * a)) || ((c * c) == (a * a) + (b * b))) {
            document.getElementById("t5").innerHTML = "It is a right angled triangle.";
        }
        document.getElementById("t4").innerHTML = "It is a scalene triangle.";
    }
}