function area1() {
    var a1 = document.getElementById("t1");
    var b1 = document.getElementById("t2");
    var c1 = document.getElementById("t3");
    var d1 = document.getElementById("t4");
    var e1 = document.getElementById("t5");
    var f1 = document.getElementById("t6");
    var g1 = document.getElementById("t7");
    var h1 = document.getElementById("t8");
    var a = parseFloat(a1.value);
    var b = parseFloat(b1.value);
    var c = parseFloat(c1.value);
    var d = parseFloat(d1.value);
    var e = parseFloat(e1.value);
    var f = parseFloat(f1.value);
    var g = parseFloat(g1.value);
    var h = parseFloat(h1.value);
    var i = Math.sqrt((((c - a) * (c - a)) + ((d - b) * (d - b))));
    var j = Math.sqrt((((c - e) * (c - e)) + ((d - f) * (d - f))));
    var k = Math.sqrt((((a - e) * (a - e)) + ((b - f) * (b - f))));
    var l = (i + j + k) / 2;
    var m = Math.sqrt((l * (l - i) * (l - j) * (l - k)));
    var n = Math.sqrt((((g - a) * (g - a)) + ((h - b) * (h - b))));
    var o = Math.sqrt((((g - e) * (g - e)) + ((h - f) * (h - f))));
    var p = Math.sqrt((((g - c) * (g - c)) + ((h - d) * (h - d))));
    var q = (n + o + k) / 2;
    var r = Math.sqrt(q * (q - n) * (q - o) * (q - k));
    var s = (n + o + p) / 2;
    var t = Math.sqrt(s * (s - n) * (s - o) * (s - p));
    var u = (o + j + p) / 2;
    var v = Math.sqrt(u * (u - o) * (u - j) * (u - p));
    if (m - (r + t + v) < 0.000001) {
        document.getElementById("p1").innerHTML = "The point P is inside the triangle.";
    }
    else {
        document.getElementById("p1").innerHTML = "The point P is outside the triangle";
    }
}
//# sourceMappingURL=app.js.map